> - **Unfortunately, this product is currently out-of-stock**
> - **Public price 99€.**
> - [Forum Premium members](https://www.ircam.fr/innovations/abonnements-du-forum/) benefit of special price 25% off  on the public price. [ Ask for the voucher](https://shop.ircam.fr/index.php?controller=contact)
> - [website](https://bitalino.com/index.php/en/r-iot-kit)
> - [Technical Support](https://bitalino.com/en/support/contactus)
> - [FAQ](https://bitalino.com/en/support/faq)

Initially developed by IRCAM ([ISMM team](http://ismm.ircam.fr)), a hacker & DIYer friendly version of this module was born thanks to the fruitful collaboration with our partner [pluX](https://plux.info): the [BITalino R-IoT ](https://bitalino.com/en/r-iot-kit)is here to fulfill your movement sensing needs with its 9 DoF IMU, featuring 3-axis accelerometer, 3-axis gyroscope and 3-axis magnetometer, all having 16-bit resolution.

   ![](https://forum.ircam.fr/media/uploads/bitalino-r-iot-pcb-front.jpg)  ![](https://forum.ircam.fr/media/uploads/bitalino-r-iot-pcb-back.jpg)

[BITalino R-IoT](https://bitalino.com/en/r-iot-kit) is a stamp-sized high resolution IMU featuring motion analysis and wireless data streaming using WiFi and Open Sound Control (OSC).

The BITalino R-IoT is powered by the mighty Texas Instruments CC3200 chipset, which also means a powerful 32-bit ARM Cortex MCU with plenty of juice and seamless Arduino-like programming through the [Energia IDE](http://energia.nu/).

The R-IoT sensor module embeds a [ST Microelectronics](https://www.st.com/en/mems-and-sensors.html) 9 axis sensor with 3 accelerometers, 3 gyroscopes and 3 magnetometers, all 16 bit. The data are sent wirelessly (WiFi) using the OSC protocol.

The core of the board is a Texas Instrument WiFi module with a 32 bit Cortex ARM processor that execute the program and deals with the Ethernet / WAN stack. It is compatible with TI’s Code Composer and with [Energia](http://energia.nu), a port of the [Arduino](https://www.arduino.cc/) environment for TI processors.

## Main Applications
Wireless gestural sensing, analysis, sensor streaming, custom gestural controller design, BITalino ecosystem WiFi transmitter to  [OpenSignal](https://bitalino.com/en/software)  software.
Operable with any OSC compliant software, including Max (examples available in [MuBu](https://forum.ircam.fr/projects/detail/mubu/) for Max).

## Technical characteristics
Dimensions : 20.5 x 34 x 5 mm (5 g + battery weight)
Power supply : 3.3 to 4.6V – 90mA @5ms sample rate (WiFi On)
Range : 50 m with a 2.4 GHz router in line-of-sight
Runtime : > 4 Hrs with a 500mAh lipo battery (could vary based on what other equipment is hooked up)

## Minimal system requirements
A 2.4 GHz WiFi router

A computer running a program or IDE compatible with Open Sound Control (Max/MSP or Pure Data for instance)

A USB port  and Virtual Com Port [FTDI](https://www.ftdichip.com/Drivers/VCP.htm) driver (for serial port configuration and text console operation)

An Ethernet port on the computer used. Though it’s possible to connect to the router by WiFi, this degrades the bandwidth and a wired connection between the computer and the router is recommended (an Ethernet / Thunderbolt adapter might be required on recent Apple MacBook laptops).

## Design and Development
The R-IoT module was designed and developed by [Emmanuel Fléty](https://www.ircam.fr/person/emmanuel-flety) on behalf of the [ISMM](http://ismm.ircam.fr) & PIP teams within the [MusicBricks](https://musictechfest.net/musicbricks/) and [RapidMix](http://rapidmix.goldsmithsdigital.com) projects. The BITalino R-IoT version was designed under supervision of [Emmanuel Fléty](https://www.ircam.fr/person/emmanuel-flety/) (IRCAM) and Rui FREIXO and Hugo SILVA (pluX).
